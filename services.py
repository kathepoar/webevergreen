from flask import Flask, jsonify, request, render_template
import requests

app = Flask(__name__, template_folder='templates')

listaValues = ['45 x 75', '40 x 30 x 25', '22 x 35']
tipoValues = ['Costal', 'Caja', 'Bolsa']

@app.route('/listarEmpaques', methods=['GET'])
def listarEmpaques():
    listaEmpaques = requests.get('http://localhost:5000/empaques').json()
    return render_template('listarEmpaques.html', empaques = listaEmpaques)


@app.route('/crearEmpaque', methods=['GET'])
def crearEmpaque():
    return render_template('crearEmpaque.html', variables=listaValues, tipos= tipoValues)


@app.route('/guardarEmpaque', methods=['POST'])
def guardarEmpaque():
    empaque = request.form.to_dict()
    requests.post('http://localhost:5000/empaques', json=empaque)
    return(listarEmpaques())

app.run(port=8000, debug=True)
